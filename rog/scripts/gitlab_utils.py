#!/usr/bin/python3

import requests
import zipfile
import io
from retry_utils import retry


API_URL = "https://gitlab.com/api/v4"


@retry()
def fetch_pipeline_bridges(headers, project_id, pipeline_id):
    response = requests.get(
        f"{API_URL}/projects/{project_id}/pipelines/{pipeline_id}/bridges",
        headers=headers,
        timeout=10,
    )
    response.raise_for_status()
    return response.json()


@retry()
def fetch_pipeline(headers, project_id, pipeline_id):
    response = requests.get(
        f"{API_URL}/projects/{project_id}/pipelines/{pipeline_id}",
        headers=headers,
        timeout=10,
    )
    response.raise_for_status()
    return response.json()


@retry()
def fetch_pipeline_jobs(headers, project_id, pipeline_id):
    response = requests.get(
        f"{API_URL}/projects/{project_id}/pipelines/{pipeline_id}/jobs", headers=headers, timeout=10
    )
    response.raise_for_status()
    return response.json()


# Use discussions if you DO want to start a thread.
# Threads must be resolved; they prevent (auto)merge.
@retry()
def post_mr_comment(headers, project_id, merge_request_iid, comment_body):
    data = {"body": comment_body}
    response = requests.post(
        f"{API_URL}/projects/{project_id}/merge_requests/{merge_request_iid}/discussions",
        headers=headers,
        json=data,
        timeout=10,
    )
    response.raise_for_status()


# Use notes if you DON'T want to start a thread.
# Threads must be resolved; they prevent (auto)merge.
@retry()
def post_mr_note(headers, project_id, merge_request_iid, comment_body):
    data = {"body": comment_body}
    response = requests.post(
        f"{API_URL}/projects/{project_id}/merge_requests/{merge_request_iid}/notes",
        headers=headers,
        json=data,
        timeout=10,
    )
    response.raise_for_status()


@retry()
def approve_mr(headers, project_id, merge_request_iid):
    response = requests.post(
        f"{API_URL}/projects/{project_id}/merge_requests/{merge_request_iid}/approve",
        headers=headers,
        timeout=10,
    )
    response.raise_for_status()


def create_and_parse_zip(byte_buffer, filename) -> str:
    """Returns string representation of needed artifact file."""
    # create a zipfile object
    # EXPLANATION: there is need to work with zipfile because
    #              even though gitlab library says that you can
    #              specify a file name and get the content of the file
    #              it doesn't work, and there is allways send a zip archive
    #              containing all artifact files
    zip_file = zipfile.ZipFile(io.BytesIO(byte_buffer), "r")

    files = zip_file.namelist()

    if filename not in files:
        print(f"File {filename} not found in zip archive.")
        exit(1)

    bytes_of_file = zip_file.read(filename)
    return bytes_of_file.decode("utf-8")


def get_single_artifact_file(job, filename):
    """Get content of a single artifact file from job in str representation"""
    # artifacts from the job in bytes
    byte_buffer = job.artifacts()
    return create_and_parse_zip(byte_buffer, filename)


def parse_artifact_content(artifact_content: str) -> dict:
    """Parse artifact content."""
    lines = artifact_content.strip().split("\n")
    content_dict = {}
    for line in lines:
        key, value = line.split("=")
        content_dict[key] = value
    return content_dict


def save_variables_from_downstream(job_name, artifact_content, result) -> None:
    """Save variables from downstream job with job_name as key."""
    content_dict = parse_artifact_content(artifact_content)
    result[job_name] = content_dict
    