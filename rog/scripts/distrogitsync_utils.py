#!/usr/bin/python3

import requests

from distgit_utils import get_commit_parents, get_downstream_hash
from retry_utils import retry

DISTROGITSYNC_URL = "https://distrogitsync.osci.redhat.com"


@retry()
def sync_mr_downstream(visibility, project_name, merge_request_iid):
    url = f"{DISTROGITSYNC_URL}/{visibility}_rpms/rpms/{project_name}/mr/{merge_request_iid}"
    response = requests.post(
        url,
        timeout=300,
    )
    response.raise_for_status()
    print(f"Sync requested successfully via {url}")


@retry()
def sync_branch_downstream(namespace, project_name, merge_request_target_branch):
    url = f"{DISTROGITSYNC_URL}/{namespace}/{project_name}/branches/{merge_request_target_branch}"
    response = requests.post(
        url,
        timeout=300,
    )
    response.raise_for_status()
    print(f"Sync requested successfully via {url}")


def validate_sync(project_name, downstream_branch, ref):
    # Inspect dist-git branch to see if the new HEAD commit is the source commit for the build.
    downstream_hash = get_downstream_hash(project_name, downstream_branch)
    if not downstream_hash:
        raise Exception("The downstream branch was not created or does not exist.")
    if ref == downstream_hash:
        print("Commits match; sync successful.")
        return
    # It's possible that the downstream HEAD is a merge commit, and we actually want to match one of the parents.
    print(f"The source commit ({ref}) for this MR doesn't match the current HEAD of {downstream_branch} ({downstream_hash}). Checking if the downstream HEAD commit is a merge commit, and if so try to match the parents.")
    parents = get_commit_parents(project_name, ref, downstream_hash)
    if len(parents) == 2:
        for parent_hash in parents:
            if parent_hash == ref:
                print(f"Parent commit ({parent_hash}) matches MR commit; sync successful.")
                return
            else:
                print(f"The parent commit ({parent_hash}) does not match the MR commit ({ref})")
        raise Exception(f"Neither parent commit matches the MR commit; sync has failed.")
    else:
        raise Exception("Commits don't match and downstream HEAD commit is not a merge commit: sync has failed.")
