#!/usr/bin/python3

import os

from distrogitsync_utils import sync_branch_downstream, validate_sync
from retry_utils import retry

namespace = os.environ.get("NAMESPACE")
project_name = os.environ.get("CI_PROJECT_NAME")
ci_commit_branch = os.environ.get("CI_COMMIT_BRANCH")
ci_commit_sha = os.environ.get("CI_COMMIT_SHA")

# Strip the c and the s from the front/end to leave the RHEL major version.
downstream_branch = f"rhel-{ci_commit_branch[1:-1]}-main"


@retry()
def sync_and_validate():
    sync_branch_downstream(namespace, project_name, ci_commit_branch)
    validate_sync(project_name, downstream_branch, ci_commit_sha)


if __name__ == "__main__":
    print(f"Attempting to sync {namespace}/{project_name} branch {ci_commit_branch} downstream.")
    sync_and_validate()
