#!/usr/bin/python3

import subprocess


DISTGIT_URL = "https://pkgs.devel.redhat.com/git/rpms"


def get_downstream_hash(project_name, ref):
    try:
        result = subprocess.run(
            ["git", "ls-remote", f"{DISTGIT_URL}/{project_name}", ref],
            text=True,
            capture_output=True,
            check=True
        )
        return result.stdout.strip().split("\t")[0]
    except subprocess.CalledProcessError as e:
        print("Error running git ls-remote:", e)
        return None


def get_commit_parents(project_name, ref, downstream_hash):
    try:
        # Unfortunately we need to shallow clone dist-git to do this
        # Make a temp repo
        subprocess.run(
            ["mkdir -p temp_repo && cd temp_repo"],
            check=True,
            shell=True
        )

        # Initialise it
        subprocess.run(
            ["git init"],
            check=True,
            shell=True
        )

        # Fetch the HEAD commit of the downstream target branch and it's
        # parents.
        subprocess.run(
            [f"git fetch {DISTGIT_URL}/{project_name} {ref}"],
            check=True,
            shell=True
        )

        # Get the parent commit hashes (first entry is HEAD)
        parents_and_head = subprocess.run(
            [f"git rev-list --parents -n 1 {downstream_hash}"],
            text=True,
            capture_output=True,
            check=True,
            shell=True
        )
        # Strip the first entry, so we only return the parents
        return parents_and_head.stdout.strip().split()[1:]
    except subprocess.CalledProcessError as e:
        print("Error determining commit parents:", e)
        raise Exception(f"Unable to determine the parents of {ref}")
    finally:
        # Tidy up
        subprocess.run(
            ["cd .. && rm -rf temp_repo"],
            check=True,
            shell=True
        )

