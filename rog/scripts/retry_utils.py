#!/usr/bin/python3

import sys
import time

from functools import wraps

def retry(max_retries=5, backoff_factor=1):
    """Decorator to retry a function."""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            for attempt in range(max_retries):
                print(f"Attempt {attempt + 1}/{max_retries} for {func.__name__}")
                try:
                    return func(*args, **kwargs)
                except Exception as e:
                    print(f"Attempt {attempt + 1}/{max_retries} for {func.__name__} failed: {e}")
                    if attempt < max_retries - 1:
                        time.sleep(backoff_factor * (2 ** attempt))
                    else:
                        print(f"Maximum number of retries ({max_retries}) met; function call failed.")
                        # Fail pipeline job if retries exhausted
                        sys.exit(1)
        return wrapper
    return decorator
