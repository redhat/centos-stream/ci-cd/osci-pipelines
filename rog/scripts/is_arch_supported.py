#!/usr/bin/python3

import os
import sys
import argparse

from specfile import Specfile


DEFAULT_ARCHES = ("x86_64", "i686")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=f"""
Read the spec file and try to determine if the package builds for {list(DEFAULT_ARCHES)} (status code 0)
or not (status code 1).
"""
    )
    parser.add_argument("--repo-dir", required=True, help="path to a repository with spec file")
    parser.add_argument("--component-name", required=True, help="component name (used for finding the spec file)")
    parser.add_argument(
        "--arch",
        required=False,
        default=f"{','.join(DEFAULT_ARCHES)}",
        help=f"a comma-separated list of arches to check"
    )

    args = parser.parse_args()

    specfile_path = os.path.join(args.repo_dir, f"{args.component_name}.spec")
    try:
        specfile = Specfile(specfile_path)
    except OSError as e:
        print(
            f"Warning: Could not open spec file {specfile_path}: {e}",
            file=sys.stderr,
        )
        sys.exit(1)

    wanted_arches = {x.strip() for x in args.arch.split(',') if x}
    print(f"Checking if {list(wanted_arches)} arches are supported...")

    arches = wanted_arches.copy()  # we assume that checked arches are supported
    exclude_arches = set()
    exclusive_arches = set()

    with specfile.tags() as tags:
        for tag in tags:
            if tag.name == "ExcludeArch":
                exclude_arches = exclude_arches.union(set(tag.expanded_value.split()))

        for tag in tags:
            if tag.name == "ExclusiveArch":
                exclusive_arches = exclusive_arches.union(set(tag.expanded_value.split()))
                break

    print(f"ExcludeArch: {list(exclude_arches)}")
    print(f"ExclusiveArch: {list(exclusive_arches)}")

    arches = arches.difference(exclude_arches)
    if exclude_arches:
        arches = arches.union(exclusive_arches)

    if arches == wanted_arches:
        print(f"{list(wanted_arches)} are supported")
        exit(0)
    else:
        print(f"{list(wanted_arches)} are not supported")

    exit(1)
