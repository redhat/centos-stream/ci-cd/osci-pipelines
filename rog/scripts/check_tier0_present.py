#!/usr/bin/python3

import sys
import os



CI_PROJECT_PATH = os.environ.get("CI_PROJECT_DIR", None)

if CI_PROJECT_PATH is None:
    print("Could not find project directory!")
    sys.exit(1)


def file_exists(file_path):
    """Check if a file exists"""
    return os.path.isfile(file_path)


def get_sti():
    """Check if a project has a sti tests
       (Checks if file tests/tests.yml exists in the project)"""
    result = file_exists(os.path.join(CI_PROJECT_PATH, "tests", "tests.yml"))
    if result:
        print("Sti tests found")
        return result
    else:
        print("No sti tests found")
    

def get_tmt():
    """Check if a project has a tmt tests
       (Checks if file fmf.version exists in the project)"""
    result = file_exists(os.path.join(CI_PROJECT_PATH, ".fmf", "version"))
    if result:
        print("Tmt tests found")
        return result
    else:
        print("No tmt tests found")

if __name__ == "__main__":
    if get_sti() or get_tmt():
        print("Sti or tmt tests found")
        with open("tier0_present.env", "w") as f:
            f.write("True")
    else:
        print("No sti or tmt tests found")
        with open("tier0_present.env", "w") as f:
            f.write("False")

