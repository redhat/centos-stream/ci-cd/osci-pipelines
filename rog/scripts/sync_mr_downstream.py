#!/usr/bin/python3

import os

from distrogitsync_utils import sync_mr_downstream, validate_sync
from retry_utils import retry


rhel_or_stream = os.environ.get("RHEL_OR_STREAM")
project_name = os.environ.get("CI_PROJECT_NAME")
merge_request_iid = os.environ.get("CI_MERGE_REQUEST_IID")
ci_merge_request_source_branch_sha = os.environ.get("CI_MERGE_REQUEST_SOURCE_BRANCH_SHA")
ci_commit_sha = os.environ.get("CI_COMMIT_SHA")


if rhel_or_stream == 'rhel':
    visibility = "private"
else:
    visibility = "public"
downstream_branch = f"private-osci-mr-{visibility}_rpms-{merge_request_iid}"

if ci_merge_request_source_branch_sha:
    ref = ci_merge_request_source_branch_sha
else:
    ref = ci_commit_sha


@retry()
def sync_and_validate():
    sync_mr_downstream(visibility, project_name, merge_request_iid)
    validate_sync(project_name, downstream_branch, ref)


if __name__ == "__main__":
    print(f"Attepting to sync {rhel_or_stream}/{project_name} MR !{merge_request_iid} downstream.")
    sync_and_validate()

