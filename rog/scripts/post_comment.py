#!/usr/bin/python3

import argparse
import os
import sys
import gitlab


def main():
    parser = argparse.ArgumentParser(
        description="Post merge request comment."
    )
    parser.add_argument("--project-id", required=True, help="GitLab Project ID")
    parser.add_argument("--merge-request-iid", required=True, help="Merge Request IID")
    parser.add_argument("--comment-file", required=True, help="A file containing the comment")
    parser.add_argument(
        "--internal", required=False, action="store_true",
        default=False, help="Make the note internal, n/a for discussions"
    )
    parser.add_argument(
        "--unique", required=False, action="store_true",
        default=False, help="Don't create the note if it already exists, n/a for discussions"
    )
    parser.add_argument(
        "--as-discussion", required=False, action="store_true",
        default=False, help="Post the comment as discussion"
    )

    args = parser.parse_args()

    if not os.getenv("GITLAB_TOKEN"):
        print("GITLAB_TOKEN is not set")
        sys.exit(1)

    with open(args.comment_file) as f:
        comment = f.read()

    if not comment:
        print(f"The comment file {args.comment_file} is empty")
        sys.exit(1)

    # This is needed for the note deduplication (--unique) to work correctly
    comment = comment.strip()

    gl = gitlab.Gitlab(private_token=os.getenv("GITLAB_TOKEN"))
    try:
        project = gl.projects.get(args.project_id)
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error: Unable to fetch project {args.project_id} - {e}")
        sys.exit(1)
    try:
        merge_request = project.mergerequests.get(args.merge_request_iid, lazy=True)
    except gitlab.exceptions.GitlabGetError as e:
        print(f"Error: Unable to fetch merge request {args.merge_request_iid} - {e}")
        sys.exit(1)

    if args.unique and not args.as_discussion:
        notes = merge_request.notes.list()
        for note in notes:
            if note.body == comment:
                print("The comment already exists, skipping...")
                sys.exit(0)

    if args.as_discussion:
        # https://docs.gitlab.com/api/discussions/#merge-requests
        merge_request.discussions.create({'body': comment})
    else:

        # https://docs.gitlab.com/api/notes/#merge-requests
        merge_request.notes.create({'body': comment, 'internal': args.internal})

if __name__ == "__main__":
    main()
